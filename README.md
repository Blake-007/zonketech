# Explanation of project structure #

All files in the app folder is 100% my code.

There are files that I have adapted from the angular.io quickstart. These files are:

* main.ts
* package.json
* systemjs.config.js
* tsconfig.json

**main.ts: ** Bootstraps the main root module of the site. This allows Angular to take over and present the site and respond to user interactions.

**package.json:** Important file that contains dependencies that are installed, which are used in the site.

**systemjs.config.js:** Loader that loads the dependencies' files for the site.

**tsconfig.json:** Compiles TypeScript and converts it to JavaScript, which is then executed in the browser

The app folder contains 2 folders: components and services

components folder contains all the site's components.

services folder contains all the site's services.

app folder contains the site's modules

### Running Site ###

In order to run the site, you need to run two commands:

* npm install - installs dependencies specified in package.json, in the project's folder.
* npm start - runs tsc and lite-server concurrently. This therefore runs the site.

Screenshots of the running site can be found in the Screenshots of site running folder

### When testing the search function: ###

The back-end contains companies that provide only 3 services, these services are: lawyers, plumbers and doctors. Therefore when testing the search function use the following keywords: plumber, lawyer, doctor