import { Injectable }    from '@angular/core';
import { Http } from '@angular/http';

import { Observable }  from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Company } from '../company';

@Injectable()
export class CompanyService {
  private companiesUrl = 'api/companies';  // URL to web api

  constructor(private http: Http) 
  { 

  }
  search(comType: string): Observable<Company[]>//searches for company details based on the type passed to it via user
  {
    return this.http
               .get(`app/companies/?type=${comType}`)
               .map(response => response.json().data as Company[]);//returns company details in the form of an array of Company objects
  }
   getCompany(id: number): Promise<Company> 
   {
    const url = `${this.companiesUrl}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as Company)
      .catch(this.handleError);
   }
  private handleError(error: any): Promise<any> //handles any errors that may occur when accessing backend
  {
    console.error('An error occurred', error); //display error in console
    return Promise.reject(error.message || error);
  }

}
