"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var Observable_1 = require("rxjs/Observable");
var Subject_1 = require("rxjs/Subject");
// Observable class extensions
require("rxjs/add/observable/of");
// Observable operators
require("rxjs/add/operator/catch");
require("rxjs/add/operator/switchmap");
var company_service_1 = require("../services/company.service");
var CompanySearchComponent = (function () {
    function CompanySearchComponent(companyService) {
        this.companyService = companyService;
        this.searchTerms = new Subject_1.Subject();
    }
    CompanySearchComponent.prototype.search = function (comType) {
        this.searchTerms.next(comType);
    };
    CompanySearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.companies = this.searchTerms
            .switchMap(function (comType) { return comType // switch to new observable each time the term changes
            ? _this.companyService.search(comType)
            : Observable_1.Observable.of([]); })
            .catch(function (error) {
            console.log(error);
            return Observable_1.Observable.of([]);
        });
    };
    return CompanySearchComponent;
}());
CompanySearchComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'search',
        templateUrl: "company-search.component.html",
        styleUrls: ["company-search.component.css"],
    }),
    __metadata("design:paramtypes", [company_service_1.CompanyService])
], CompanySearchComponent);
exports.CompanySearchComponent = CompanySearchComponent;
//# sourceMappingURL=company-search.component.js.map