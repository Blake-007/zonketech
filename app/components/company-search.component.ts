import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';
import { Observable }        from 'rxjs/Observable';

import { Subject }           from 'rxjs/Subject';
// Observable class extensions
import 'rxjs/add/observable/of';
// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchmap';

import {Company} from '../company';
import {CompanyService} from '../services/company.service';

@Component({
  moduleId: module.id,
  selector: 'search',
  templateUrl: `company-search.component.html`,
  styleUrls: [`company-search.component.css`],
})
export class CompanySearchComponent implements OnInit  {
    
    companies: Observable<Company[]>;
    private searchTerms = new Subject<string>();
    constructor(private companyService: CompanyService)
    {
        
    }
    search(comType: string): void
    {
        this.searchTerms.next(comType);
    }
    
    ngOnInit(): void 
    {
        this.companies = this.searchTerms
      .switchMap(comType => comType   // switch to new observable each time the term changes
        // return the http search observable
        ? this.companyService.search(comType)
        // or the observable of empty companies if there was no search term
        : Observable.of<Company[]>([]))
      .catch(error => {
        console.log(error);
        return Observable.of<Company[]>([]);
      });

    }
 }
 