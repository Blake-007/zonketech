"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var CompanyAppComponent = (function () {
    function CompanyAppComponent(router) {
        this.router = router;
    }
    CompanyAppComponent.prototype.ngOnInit = function () {
        //ensures every component scrolls to the top of window when loaded
        this.router.events.subscribe(function (evt) {
            if (!(evt instanceof router_1.NavigationEnd)) {
                return;
            }
            window.scrollTo(0, 0);
        });
    };
    return CompanyAppComponent;
}());
CompanyAppComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'my-app',
        templateUrl: "company-app.component.html",
        styleUrls: ["company-app.component.css"]
    }),
    __metadata("design:paramtypes", [router_1.Router])
], CompanyAppComponent);
exports.CompanyAppComponent = CompanyAppComponent;
//# sourceMappingURL=company-app.component.js.map