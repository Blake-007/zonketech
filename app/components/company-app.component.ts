import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: `company-app.component.html`,
  styleUrls: [`company-app.component.css`]
})
export class CompanyAppComponent implements OnInit  
{
    constructor(private router: Router) 
    {
        
    }

    ngOnInit()
    {
         //ensures every component scrolls to the top of window when loaded
        this.router.events.subscribe((evt) => 
        {
            if (!(evt instanceof NavigationEnd)) 
            {
                return;
            }
            window.scrollTo(0, 0)
        });
    }
}
